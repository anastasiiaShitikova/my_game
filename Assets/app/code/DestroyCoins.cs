﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyCoins : MonoBehaviour
{
    public GameObject Coin;

    public ParticleSystem Bonus;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Bonus.transform.position = Coin.transform.position;
            Bonus.Play();
            Bonus.enableEmission = true;
            Destroy(Coin);
        }
    }
}
